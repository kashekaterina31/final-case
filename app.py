from flask import Flask, render_template, request
import pickle

app = Flask(__name__)

# Load the model only once
with open('rfr_pkl.pkl', 'rb') as file:
    loaded_model = pickle.load(file)

@app.route('/', methods=["get", "post"])
def predict():

    prediction = ""
    if request.method == "POST":
        IW = float(request.form.get("IW"))
        IF = float(request.form.get("IF"))
        VW = float(request.form.get("VW"))
        FP = float(request.form.get("FP"))

        person = [IW, IF, VW, FP]
        pred = loaded_model.predict([person])
        prediction = "Результат: {0}".format(pred[0])

        print(f"Width_prediction: {pred[0][0]}")
        print(f"Depth_prediction: {pred[0][1]}")

    return render_template("index.html", prediction=prediction)


if __name__ == "__main__":
    app.run()